from DAL.DBContext import DBContext
from Model.API.Response import DueBusListResponse
from Mqtt.MqttPublisher import MqttPublisher

class BusStationBL:
    def __init__(self, context:DBContext, mqttPub: MqttPublisher):
        self._context = context
        self._mqttPub = mqttPub

    def GetDueBusList(self,StationCode):
        querry = {"Code":StationCode}
        stationList = self._context.BusStationCollection.find_one(querry)
        
        dicList = list()
        for due in list(stationList['DueBusList']):
            dicList.append(self.GetBus(due))
        print(dicList)
        return dicList

    def GetBus(self,BusCode):
        querry = {"Code":BusCode}
        bus = self._context.BusCollection.find_one(querry)
        bus_ = DueBusListResponse(
                BusCode = bus['Code'],
                BusNumber = bus['BusNumber'],
                Towards = bus['Towards'],
                StopRequest = 'Yes' if bus['BoardingRequest']  or  bus['StopRequest'] else 'No',
                Occupancy = str(bus['Occupancy'])
            )
        print(bus_)
        return bus_.__dict__
    
    def UpdateBoardRequest(self,message):
        querry = {"Code":message['BusId']}
        update = {"$set":{"BoardingRequest" : message['StopRequest'] == 1}}
        self._context.BusCollection.update_one(querry,update)
        self._mqttPub.Publish("data","ptas/bus/notification/"+message['BusId'])

