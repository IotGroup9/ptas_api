from DAL.DBContext import DBContext
from Model.API.Response import BusListResponse
from Mqtt.MqttPublisher import MqttPublisher

class BusBL:
    def __init__(self, context:DBContext, mqttPub: MqttPublisher):
        self._context = context
        self._mqttPub = mqttPub

    def GetBusList(self):
        busList = self._context.BusCollection.find()
        dicList = list()
        for bus in busList:
            bus_ = BusListResponse(
                BusNumber = bus['BusNumber'],
                PointA = bus['PointA'],
                PointB = bus['PointB'],
                Towards = bus['Towards'],
                NextStop = bus['NextStop'],
                StopRequest = 'Yes' if bus['BoardingRequest']  and  bus['StopRequest'] else 'No',
                Capacity = bus['Capacity'],
                Occupancy = bus['Occupancy']
            )
            dicList.append(bus_.__dict__)
        return dicList

    def GetBus(self,BusCode):
        querry = {"Code":BusCode}
        bus = self._context.BusCollection.find_one(querry)
        bus_ = BusListResponse(
                BusNumber = bus['BusNumber'],
                PointA = bus['PointA'],
                PointB = bus['PointB'],
                Towards = bus['Towards'],
                NextStop = bus['NextStop'],
                StopRequest = 'Yes' if bus['BoardingRequest']  or  bus['StopRequest'] else 'No',
                Capacity = bus['Capacity'],
                Occupancy = bus['Occupancy']
            )
        return bus_.__dict__

    def GetBrokerCredentials(self,BusCode):
        querry = {"Code":BusCode}
        busList = self._context.BusCollection.find(querry)
        if busList.count() == 1:
            return True
        else:
            return False

    def UpdateBusOccupancy(self,message,BusCode):
        try:
            querry = {"Code":BusCode}
            busList = self._context.BusCollection.find_one(querry)
            Occupancy = (busList['Capacity'] - 1) if busList['Capacity'] < message['Occupancy'] else message['Occupancy']
            update = {"$set":{"Occupancy" :  Occupancy}}
            self._context.BusCollection.update_one(querry,update)
             #publish the message to the bus station
            self._mqttPub.Publish(busList['NextStopId'],"ptas/busStation/notification/"+busList['NextStopId'])
        except Exception as identifier:
            print(identifier)
    
    def UpdaeBusStopRequest(self,message,BusCode):
        querry = {"Code":BusCode}
        update = {"$set":{"StopRequest" : message['StopRequest'] == 1}}
        self._context.BusCollection.update_one(querry,update)

    def UpdateNextStop(self,message,BusCode):
        try:
            #remove due from previous stop
            querry = {"Code": message['PreviousStop']}
            busStation = self._context.BusStationCollection.find_one(querry)
            dueList = list(busStation['DueBusList'])
            if BusCode in dueList:
                dueList.remove(BusCode)
            update = {"$set":{"DueBusList" : dueList}}
            self._context.BusStationCollection.update_one(querry,update)
            self._mqttPub.Publish("data","ptas/busStation/notification/"+message['PreviousStop'])
            #add due to the next stop
            querry = {"Code": message['NextStop']}
            busStation = self._context.BusStationCollection.find_one(querry)
            dueList = list(busStation['DueBusList'])
            if not BusCode in dueList:
                dueList.append(BusCode)
            update = {"$set":{"DueBusList" : dueList}}
            self._context.BusStationCollection.update_one(querry,update)
            
            #update next stop in bus
            querry = {"Code":BusCode}
            update = {"$set":{"NextStopId": busStation['Code'], "NextStop" : busStation['StationName'] }}
            self._context.BusCollection.update_one(querry,update)

            #publish the message to the bus station
            self._mqttPub.Publish("data","ptas/busStation/notification/"+message['NextStop'])
        except Exception as identifier:
            print(identifier)
        
