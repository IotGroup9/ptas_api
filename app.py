from flask import Flask
from flask_restful import Api, Resource, reqparse
from Controller.BusController import BusListResource, BrokerCredentialsResource
from Controller.BusStationController import DueBusListResource
app = Flask(__name__)
api = Api(app)

api.add_resource(BusListResource, "/bus/list")
api.add_resource(BrokerCredentialsResource, "/bus/brokerCred")
api.add_resource(DueBusListResource,"/busStation/dueList")

app.run(host='0.0.0.0' , port=8080)