from flask_restful import Api, Resource, reqparse
from flask import jsonify
from DI import DependencyContainer

class DueBusListResource(Resource):
    def __init__(self):
        self._businessLogic = DependencyContainer.busStationBLService()

    def get(self):
        try:
            return self._businessLogic.GetDueBusList("BS0004"), 200
        except Exception as ex:
            return ex, 500
