from flask_restful import Api, Resource, reqparse
from BusinessLogic.BusBL import BusBL
from DI import DependencyContainer
from flask import jsonify, make_response

class BusListResource(Resource):

    def __init__(self):
        self._businessLogic = DependencyContainer.busBLService()

    def get(self):
        try:
            response = self._businessLogic.GetBusList()
            print(response)
            return response,200
        except Exception as ex:
            return ex, 500

class BusDetailsResource(Resource):
    def __init__(self):
        self._businessLogic = DependencyContainer.busBLService()

    def get(self):
        try:
            return self._businessLogic.GetBus("B0001"), 200
        except Exception as ex:
            return jsonify(ex), 500

class BrokerCredentialsResource(Resource):

    def __init__(self):
        self._businessLogic = DependencyContainer.busBLService()

    def get(self):
        try:
            return self._businessLogic.GetBrokerCredentials("B0001"), 200
        except Exception as ex:
            return ex.__dict__, 500 

            