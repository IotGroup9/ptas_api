import paho.mqtt.client as mqtt
from DI import DependencyContainer
import json

class Subscriber:
    host = 'broker.mqttdashboard.com'
    port = 1883
    def __init__(self):
        self._client = mqtt.Client("h")
        self._client.connect(self.host,1883,60)
        self._client.on_connect = self.on_connect
        self._client.on_message = self.on_message
        self._client.loop_forever()

    # def __del__(self):
    #     self._client.lo

    def on_connect(self,client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        client.subscribe("ptas/#")

    def on_message(self,client, userdata, message):
        try:
            msg = json.loads(str(message.payload.decode("utf-8","ignore")))
            DependencyContainer.mqttRouteService().Routing(message.topic,msg)
        except Exception as identifier:
            print(identifier)

if __name__ == "__main__":
    Subscriber()