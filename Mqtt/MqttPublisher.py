import paho.mqtt.client as publisher

class MqttPublisher (object):

    host = 'broker.mqttdashboard.com'
    port = 1883

    def __init__(self):
        self._clientId = "S"

    def Publish(self,data,topic):
        self._client = publisher.Client(self._clientId)
        self._client.on_publish = self.on_publish
        self._client.connect(self.host,self.port)
        print(topic,data)
        self._client.publish(topic,data)
        self._client.disconnect()

    def on_publish(self,client,userdata,result):             #create function for callback
        print("data published \n")