from BusinessLogic.BusBL import BusBL
from BusinessLogic.BusStationBL import BusStationBL

class RouteHandler:

    def __init__(self,busBL:BusBL,stationBL:BusStationBL):
        self._busBL = busBL
        self._stationBL = stationBL

    def Routing(self,topic : str,message):
        topicArray = topic.split('/')
        if topicArray[0].lower() == 'ptas':
            print(topic)
            if topicArray[1].lower() == 'bus':
                if topicArray[2].lower() == 'occupancy':
                    self._busBL.UpdateBusOccupancy(message,topicArray[3])
                if topicArray[2].lower() == 'stoprequest':
                    self._busBL.UpdaeBusStopRequest(message,topicArray[3])
                if topicArray[2].lower() == 'nextstop':
                    self._busBL.UpdateNextStop(message,topicArray[3])
                    
            if topicArray[1].lower == 'busstation':
                if topicArray[2].lower() == 'boardrequest':
                    self._stationBL.UpdateBoardRequest(message)
