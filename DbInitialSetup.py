import pymongo    

Client = pymongo.MongoClient("mongodb://localhost:27017")
dataBase = Client['ptas_db']
BusCollection = dataBase['BusCollection']
BusStationCollection = dataBase['BusStationCollection']

#drop all existing collections
if BusCollection.drop() :
    BusCollection = dataBase['BusCollection']

if BusStationCollection.drop() :
    BusCollection = dataBase['BusStationCollection']


#insert data into Bus collection
BusCollection.insert_many(
    [
        {
            "Code":"B0001",
            "BusNumber":"150",
            "PointAId":"BS0010",
            "PointBId":"BS0011",
            "TowardsId":"BS0011",
            "PointA":"Clogher Rd",
            "PointB":"Rossmore",
            "Towards":"Rossmore",
            "NextStopId":"BS0001",
            "BoardingRequest":False,
            "StopRequest":False,
            "NextStop":"Hawkins St",
            "Capacity":75,
            "Occupancy":10
        },
        {
            "Code":"B0002",
            "BusNumber":"7",
            "PointAId":"BS0012",
            "PointBId":"BS0013",
            "TowardsId":"O'Connell St",
            "PointA":"Mountjoy Sq",
            "PointB":"Brides Glen Luas Stop",
            "Towards":"Brides Glen Luas Stop",
            "NextStopId":"BS0004",
            "BoardingRequest":False,
            "StopRequest":False,
            "NextStop":"O'Connell St",
            "Capacity":75,
            "Occupancy":30
        },
        {
            "Code":"B0003",
            "BusNumber":"27a",
            "PointAId":"BS0014",
            "PointBId":"BS0015",
            "TowardsId":"BS0015",
            "PointA":"Eden Quay",
            "PointB":"Blunden Drive",
            "Towards":"Blunden Drive",
            "NextStopId":"BS0014",
            "BoardingRequest":False,
            "StopRequest":False,
            "NextStop":"Eden Quay",
            "Capacity":75,
            "Occupancy":0
        }
    ]
)

BusStationCollection.insert_many(
    [
        {
            "Code":"BS0001",
            "StationName": "Hawkins St",
            "DueBusList": ["B0001"]
        },
        {
            "Code":"BS0002",
            "StationName": "Werburgh St",
            "DueBusList": []
        },
        {
            "Code":"BS0003",
            "StationName": "Kevin St",
            "DueBusList": []
        },
        {
            "Code":"BS0004",
            "StationName": "O'Connell St",
            "DueBusList": ["B0002"]
        },
        {
            "Code":"BS0005",
            "StationName": "Clare St",
            "DueBusList": []
        },
        {
            "Code":"BS0006",
            "StationName": "Merrion Sq",
            "DueBusList": []
        },
        {
            "Code":"BS0007",
            "StationName": "Connolly Rail Station",
            "DueBusList": []
        },
        {
            "Code":"BS0008",
            "StationName": "Newcomen Bridge",
            "DueBusList": []
        },
        {
            "Code":"BS0009",
            "StationName": "Annesley Bridge Road",
            "DueBusList": []
        },
        {
            "Code":"BS0010",
            "StationName": "Clogher Rd",
            "DueBusList": []
        },
        {
            "Code":"BS0011",
            "StationName": "Rossmore",
            "DueBusList": []
        },
        {
            "Code":"BS0012",
            "StationName": "Mountjoy Sq",
            "DueBusList": []
        },
        {
            "Code":"BS0013",
            "StationName": "Brides Glen Luas Stop",
            "DueBusList": []
        },
        {
            "Code":"BS0014",
            "StationName": "Eden Quay",
            "DueBusList": ["B0003"]
        },
        {
            "Code":"BS0015",
            "StationName": "Blunden Drive",
            "DueBusList": []
        }
    ]
)