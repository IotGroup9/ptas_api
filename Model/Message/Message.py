class PostOccupancyMessage:
    Code = str()
    Occupancy = int()

class GetNotificationMessage:
    StopRequest = int()

class PostStopRequestMessage:
    Code = str()
    StopRequest = int()

class BusListMessage:
    Buslist = list()

class PostBoardRequestMessage:
    Code = str()
    BusId = str()
    StopRequest = int()