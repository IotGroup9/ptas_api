
class BusListResponse:
    def __init__(self,**kwargs):
        self.BusNumber = kwargs.get('BusNumber')
        self.PointA = kwargs.get('PointA')
        self.PointB = kwargs.get('PointB')
        self.Towards = kwargs.get('Towards')
        self.NextStop = kwargs.get('NextStop')
        self.StopRequest = kwargs.get('StopRequest')
        self.StopRequest = kwargs.get('StopRequest')
        self.Capacity = kwargs.get('Capacity')
        self.Occupancy = kwargs.get('Occupancy')

class DueBusListResponse:
    def __init__(self,**kwargs):
        self.BusCode = kwargs.get('BusCode')
        self.BusNumber = kwargs.get('BusNumber')
        self.Towards = kwargs.get('Towards')
        self.StopRequest = kwargs.get('StopRequest')
        self.Occupancy = kwargs.get('Occupancy')