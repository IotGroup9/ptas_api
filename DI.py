from dependency_injector import containers, providers
from DAL.DBContext import DBContext
from BusinessLogic.BusBL import BusBL
from BusinessLogic.BusStationBL import BusStationBL
from Mqtt.RouteHandler import RouteHandler
from Mqtt.MqttPublisher import MqttPublisher


class DependencyContainer (containers.DeclarativeContainer):

    #Adding Services
    contextService = providers.Factory(DBContext)
    mqttPublishingService = providers.Factory(MqttPublisher)
    busBLService = providers.Factory(BusBL,contextService,mqttPublishingService)
    busStationBLService = providers.Factory(BusStationBL,contextService,mqttPublishingService)
    mqttRouteService = providers.Factory(RouteHandler,busBLService,busStationBLService)